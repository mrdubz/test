$(document).ready(function () {
  $('.slider__list').slick({
    //fade: true,
    dots: true,
    infinite: false,
    prevArrow: '.slider__controls--prev',
    nextArrow: '.slider__controls--next',
    appendDots: '.slider__controls--dots'
  });
  $('.js-schedule').click(function () {
    $(this).closest('.schedule__item').toggleClass('open');
  });
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
  $('.chart.teal').easyPieChart({
    size: 200,
    barColor: "#009981",
    scaleLength: 0,
    lineWidth: 32,
    trackColor: "#E6E6E6",
    lineCap: "circle",
    animate: 1000,
  });
  $('.chart.red').easyPieChart({
    size: 200,
    barColor: "#FF4949",
    scaleLength: 0,
    lineWidth: 32,
    trackColor: "#E6E6E6",
    lineCap: "circle",
    animate: 1000,
  });
});
